---
title: "Spline Quadrático"
date: 2021-10-24T12:10:19-03:00
draft: false
---
<center><img src="../../images/spline_quad.PNG"></center>

Suponha que o intervalo [a, b] seja [0, 3] e que os subintervalos sejam [0, 1], [1, 2] e [2, 3]. Suponha que os polinômios sejam quadráticos e que a função nos primeiros dois subintervalos devem ser iguais em valor e variação na sua junção e que nos últimos dois a junção deve ser igual somente em valor. Uma S(t) que atende tais determinações:

<center><img class="equation" src="../../images/ex1-first.svg" alt="Definição do Spline"></center>
