---
title: "Gráfico de um Spline"
date: 2021-10-24T12:10:32-03:00
draft: false
---

O seguinte Spline:

<center><img class="equation" src="../../images/ex2-first.svg" alt="Definição do Spline"></center>

apresenta a forma:

<center><img src="../../images/spline_cub.PNG"></center>