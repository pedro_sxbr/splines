---
title: "Spline Cubico"
date: 2021-10-24T12:10:26-03:00
draft: false
---

Segue o spline cúbico natural que interpola os pontos C = [(0,1),(2,1/4),(3/4,0),(1,1)]:

<center><img class="equation" src="../../images/ex2-first.svg" alt="Definição do Spline"></center>
