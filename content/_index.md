Em matemática, um spline é uma função especial definida por partes por polinômios. Normalmente a interpolação por splines é preferível em relação à interpolação polinomial por retornar resultados similares quando usando polinômios de baixa ordem e evitar problemas que aparecem na interpolação polinomial quando utilizando polinômios de alta ordem.

Splines são curvas paramétricas muito utilizadas na área de ciência da computação e de design gráfico, uma vez que são curvas simples de serem construídas, a precisão de sua avaliação e a sua capacidade de aproximar curvas complexas.

## Definição

Seja S uma função tal que:

<center><img class="equation" src="images/firsteq.svg" alt="função S de [a,b] em R"></center>

Como é necessário que S seja definida por partes, então o domínio deve ser coberto por k subintervalos ordenados

<center><img class="equation" src="images/secdeq.svg" alt="definição de subintervalos"></center>

Para cada um desses subintervalos, define-se um polinomial

<center><img class="equation" src="images/thrdeq.svg" alt="Polinômio de subintervalo em R"></center>

no *i-ézimo* subintervalo de *[a,b]*, S é definido pelo *i-ézimo* polinômio.

<center><img class="equation" src="images/frtheq.svg" alt="Definição de S(t)"></center>

Se o polinômio de maior ordem dentre os k poliômios definidos nos subintervalos de *[a,b]* for de ordem n, então é dito que o spline é de ordem n. Splines cubicos (de ordem 3) são normalmente os mais utilizados.